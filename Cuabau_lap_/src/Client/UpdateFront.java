/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Client;

import Client.model.User;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JLabel;

/**
 *
 * @author MSI
 */
public class UpdateFront extends Thread {

    public UpdateFront(Socket socket, DataInputStream dis, DataOutputStream dos, User user, JLabel jLabel, JLabel tongtienUserView, JButton btnbau, JButton btncua, JButton btntom, JButton btnca, JButton btnnai, JButton btnga, JLabel xbau, JLabel xcua, JLabel xtom, JLabel xca, JLabel xnai, JLabel xga, JLabel xx1, JLabel xx2, JLabel xx3, int[] cuoc, JButton exit) {
        this.socket = socket;
        this.dis = dis;
        this.dos = dos;
        this.user = user;
        this.jLabel = jLabel;
        this.tongtienUserView = tongtienUserView;
        this.btnbau = btnbau;
        this.btncua = btncua;
        this.btntom = btntom;
        this.btnca = btnca;
        this.btnnai = btnnai;
        this.btnga = btnga;
        this.xbau = xbau;
        this.xcua = xcua;
        this.xtom = xtom;
        this.xca = xca;
        this.xnai = xnai;
        this.xga = xga;
        this.xx1 = xx1;
        this.xx2 = xx2;
        this.xx3 = xx3;
        this.cuoc = cuoc;
        this.exit = exit;
    }

    private Socket socket;
    private DataInputStream dis;
    private DataOutputStream dos;
    private User user;
    private JLabel jLabel;
    private JLabel tongtienUserView;
    private JButton btnbau;
    private JButton btncua;
    private JButton btntom;
    private JButton btnca;
    private JButton btnnai;
    private JButton btnga;
    private JLabel xbau;
    private JLabel xcua;
    private JLabel xtom;
    private JLabel xca;
    private JLabel xnai;
    private JLabel xga;
    private JLabel xx1;
    private JLabel xx2;
    private JLabel xx3;
    private int[] cuoc;
    private JButton exit;

    public void run() {
        while (true) {
            try {
                String type = dis.readUTF();
                System.out.println(type);
                if (type.equals("mess")) {
                    String m = dis.readUTF();
                    if (m.equals("stop")) {
                        System.out.println("stop");
                        // disable
                        btnbau.setEnabled(false);
                        btncua.setEnabled(false);
                        btntom.setEnabled(false);
                        btnca.setEnabled(false);
                        btnnai.setEnabled(false);
                        btnga.setEnabled(false);
                        this.xx1.setVisible(false);
                        this.xx2.setVisible(false);
                        this.xx3.setVisible(false);
                    }
          //          this.exit.setEnabled(false);

                }
                if (type.equals("XxBack")) {
                    int xx1 = dis.readInt();
                    int xx2 = dis.readInt();
                    int xx3 = dis.readInt();
                    int xx[] = {xx1, xx2, xx3};
                    for (int i = 0; i < 3; i++) {
                        String s = "";
                        switch (xx[i]) {
                            case 0:
                                s = "BAU";
                                break;
                            case 1:
                                s = "CUA";
                                break;
                            case 2:
                                s = "TOM";
                                break;
                            case 3:
                                s = "CA";
                                break;
                            case 4:
                                s = "NAI";
                                break;
                            case 5:
                                s = "GA";
                                break;
                        }
                        if (i == 0) {
                            this.xx1.setText(s);
                        }
                        if (i == 1) {
                            this.xx2.setText(s);
                        }
                        if (i == 2) {
                            this.xx3.setText(s);
                        }
                    }
                    for (int i = 0; i < 6; i++) {
                        cuoc[i] = 0;
                    }
                    this.xx1.setVisible(true);
                    this.xx2.setVisible(true);
                    this.xx3.setVisible(true);
                 //   this.exit.setEnabled(true);
                }
                if (type.equals("MoneyBack")) {
                    btnbau.setEnabled(true);
                    btncua.setEnabled(true);
                    btntom.setEnabled(true);
                    btnca.setEnabled(true);
                    btnnai.setEnabled(true);
                    btnga.setEnabled(true);
                    int back = dis.readInt();
                    back += user.getMoney();
                    jLabel.setText("$$: " + String.valueOf(back));
                    user.setMoney(back);
                    tongtienUserView.setText("Tiền của bạn: " + String.valueOf(back));
                    xbau.setText("x0");
                    xcua.setText("x0");
                    xtom.setText("x0");
                    xca.setText("x0");
                    xnai.setText("x0");
                    xga.setText("x0");
               //     this.exit.setEnabled(true);

                }
            } catch (IOException ex) {
                Logger.getLogger(UpdateFront.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }
}
