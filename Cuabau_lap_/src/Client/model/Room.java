package Client.model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import Server.ServerRoom;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
/**
 *
 * 
 */
public class Room implements Serializable{
    private User user;
    private HashSet<ServerRoom> userInRoom;

    public Room( User user) {
        this.user = user;
        this.userInRoom = new HashSet<ServerRoom>();
    }
    
    public int numberUserInRoom(){
        return userInRoom.size();
    }
    
    public void addUser(ServerRoom e){
        this.userInRoom.add(e);
    }

    public HashSet<ServerRoom> getUserInRoom() {
        return userInRoom;
    }

    public User getUser() {
        return user;
    }
    
    public void removeUser(ServerRoom srm){
        userInRoom.remove(srm);
    }
}
