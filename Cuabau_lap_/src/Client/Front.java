package Client;

import static Client.Front.InRoomGame;
import static Client.Front.roomName;
import static Client.Front.user;
import static Client.Login.socket;
import Client.model.User;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.HashSet;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Front extends javax.swing.JFrame {

    public static int cuoc[] = {0, 0, 0, 0, 0, 0};
    public static int xx[] = {0, 0, 0};
    public static int check = 0;

    public static User user;
    public static String roomName;
    public Socket socket;
    public Socket socketUV;

    public DataInputStream dis;
    public DataOutputStream dos;
    public static HashSet<String> InRoomGame;

    public Front(User user, String roomname, HashSet<String> inroomGame, JLabel tongtienUserView, Socket socketUV) throws IOException {
        initComponents();
        this.user = user;
        this.roomName = roomname;
        this.socketUV = socketUV;
        socket = new Socket("localhost", 1107);
        dis = new DataInputStream(socket.getInputStream());
        dos = new DataOutputStream(socket.getOutputStream());
        //     jLabel3.setText(roomName);
        dos.writeUTF("addUserToRoom");
        dos.writeUTF(user.getName());
        dos.writeUTF(roomName);
//        dos.writeUTF("mess");
//        dos.writeUTF(user.getName() + " đã tham gia phòng chat ...");
        //      jLabel4.setText(user.getName());ss
       // System.out.println(user.getName());
        tenuser.setText("Tên người chơi: " + user.getName());
        tongtien.setText("$$: " + user.getMoney());
        this.setTitle("Phòng của " + roomname);

        this.InRoomGame = inroomGame;
        new UpdateFront(socket, dis, dos, user, tongtien, tongtienUserView, bau, cua, tom, ca, nai, ga, xbau, xcua, xtom, xca, xnai, xga, xx1, xx2, xx3, cuoc,exit).start();
    }
//    public Front() {
//        initComponents();
//    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {
        ImageIcon imgbau = new ImageIcon("C:\\Users\\Admin\\Desktop\\anh\\bau.png");
        ImageIcon imgcua = new ImageIcon("C:\\Users\\Admin\\Desktop\\anh\\cua.png");
        ImageIcon imgtom = new ImageIcon("C:\\Users\\Admin\\Desktop\\anh\\tom.png");
        ImageIcon imgca = new ImageIcon("C:\\Users\\Admin\\Desktop\\anh\\ca.png");
        ImageIcon imgnai = new ImageIcon("C:\\Users\\Admin\\Desktop\\anh\\nai.png");
        ImageIcon imgga = new ImageIcon("C:\\Users\\Admin\\Desktop\\anh\\ga.png");
        ImageIcon imgxx = new ImageIcon("C:\\Users\\Admin\\Desktop\\anh\\xocbat.png");
        ImageIcon imgtien = new ImageIcon("C:\\Users\\Admin\\Desktop\\anh\\tien1.png");
        bau = new javax.swing.JButton(imgbau);
        cua = new javax.swing.JButton(imgcua);
        tom = new javax.swing.JButton(imgtom);
        ca = new javax.swing.JButton(imgca);
        nai = new javax.swing.JButton(imgnai);
        ga = new javax.swing.JButton(imgga);
        play = new javax.swing.JButton();
        exit = new javax.swing.JButton();
        tenuser = new javax.swing.JLabel();
        tongtien = new javax.swing.JLabel();
        tien = new javax.swing.JLabel(imgtien);
        tbau = new javax.swing.JLabel(imgtien);
        tcua = new javax.swing.JLabel(imgtien);
        ttom = new javax.swing.JLabel(imgtien);
        tca = new javax.swing.JLabel(imgtien);
        tnai = new javax.swing.JLabel(imgtien);
        tga = new javax.swing.JLabel(imgtien);
        xbau = new javax.swing.JLabel();
        xcua = new javax.swing.JLabel();
        xtom = new javax.swing.JLabel();
        xca = new javax.swing.JLabel();
        xnai = new javax.swing.JLabel();
        xga = new javax.swing.JLabel();
        dia = new ImagePanel(new ImageIcon("C:\\Users\\Admin\\Desktop\\anh\\dia.png").getImage());
        xx1 = new javax.swing.JLabel();
        xx2 = new javax.swing.JLabel();
        xx3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        bau.setPreferredSize(new java.awt.Dimension(150, 150));
        bau.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    bauActionPerformed(evt);
                } catch (IOException ex) {
                    Logger.getLogger(Front.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        cua.setPreferredSize(new java.awt.Dimension(150, 150));
        cua.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    cuaActionPerformed(evt);
                } catch (IOException ex) {
                    Logger.getLogger(Front.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        tom.setPreferredSize(new java.awt.Dimension(150, 150));
        tom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    tomActionPerformed(evt);
                } catch (IOException ex) {
                    Logger.getLogger(Front.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        ca.setPreferredSize(new java.awt.Dimension(150, 150));
        ca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    caActionPerformed(evt);
                } catch (IOException ex) {
                    Logger.getLogger(Front.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        nai.setPreferredSize(new java.awt.Dimension(150, 150));
        nai.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    naiActionPerformed(evt);
                } catch (IOException ex) {
                    Logger.getLogger(Front.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        ga.setPreferredSize(new java.awt.Dimension(150, 150));
        ga.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    gaActionPerformed(evt);
                } catch (IOException ex) {
                    Logger.getLogger(Front.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

//        play.setText("Play");
//        play.addActionListener(new java.awt.event.ActionListener() {
//            public void actionPerformed(java.awt.event.ActionEvent evt) {
//                playActionPerformed(evt);
//            }
//        });
        exit.setText("Exit");
        exit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitActionPerformed(evt);
            }
        });

        tenuser.setText("Tên người chơi: ");
        tenuser.setPreferredSize(new java.awt.Dimension(25, 23));
        tongtien.setText("$$: ");
        tongtien.setPreferredSize(new java.awt.Dimension(51, 23));

        tien.setPreferredSize(new java.awt.Dimension(30, 23));

        xbau.setText("x0");
        xbau.setPreferredSize(new java.awt.Dimension(20, 14));

        xcua.setText("x0");
        xcua.setPreferredSize(new java.awt.Dimension(20, 14));

        xtom.setText("x0");
        xtom.setPreferredSize(new java.awt.Dimension(20, 14));

        xca.setText("x0");
        xca.setPreferredSize(new java.awt.Dimension(20, 14));

        xnai.setText("x0");
        xnai.setPreferredSize(new java.awt.Dimension(20, 14));

        xga.setText("x0");
        xga.setPreferredSize(new java.awt.Dimension(20, 14));

        dia.setPreferredSize(new java.awt.Dimension(250, 250));

        xx1.setPreferredSize(new java.awt.Dimension(50, 50));
        xx2.setPreferredSize(new java.awt.Dimension(50, 50));
        xx3.setPreferredSize(new java.awt.Dimension(50, 50));

        javax.swing.GroupLayout diaLayout = new javax.swing.GroupLayout(dia);
        dia.setLayout(diaLayout);
        diaLayout.setHorizontalGroup(
                diaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(diaLayout.createSequentialGroup()
                                .addGap(49, 49, 49)
                                .addComponent(xx3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(52, 52, 52)
                                .addGroup(diaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(xx2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(xx1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(49, Short.MAX_VALUE))
        );
        diaLayout.setVerticalGroup(
                diaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(diaLayout.createSequentialGroup()
                                .addGap(57, 57, 57)
                                .addGroup(diaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(diaLayout.createSequentialGroup()
                                                .addComponent(xx1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(36, 36, 36)
                                                .addComponent(xx2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(diaLayout.createSequentialGroup()
                                                .addGap(43, 43, 43)
                                                .addComponent(xx3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addContainerGap(57, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(bau, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addGroup(layout.createSequentialGroup()
                                                                                .addComponent(tbau)
                                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                                .addComponent(xbau, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                                .addGap(18, 18, 18)
                                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(cua, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addGroup(layout.createSequentialGroup()
                                                                                .addComponent(tcua)
                                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                                .addComponent(xcua, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                                .addGap(18, 18, 18)
                                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                                        .addGroup(layout.createSequentialGroup()
                                                                                .addComponent(ttom)
                                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                                .addComponent(xtom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                        .addComponent(tom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(ca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addGroup(layout.createSequentialGroup()
                                                                                .addComponent(tca)
                                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                                .addComponent(xca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                                .addGap(18, 18, 18)
                                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(nai, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addGroup(layout.createSequentialGroup()
                                                                                .addComponent(tnai)
                                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                                .addComponent(xnai, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                                .addGap(18, 18, 18)
                                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                                        .addGroup(layout.createSequentialGroup()
                                                                                .addComponent(tga)
                                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                                .addComponent(xga, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                        .addComponent(ga, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                                .addGap(0, 0, Short.MAX_VALUE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addComponent(exit)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addComponent(tongtien, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addGap(127, 127, 127)
                                                                .addComponent(dia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(0, 0, Short.MAX_VALUE)))
                                                .addGap(28, 28, 28)
                                                .addComponent(tien, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addContainerGap())
                        .addGroup(layout.createSequentialGroup()
                                .addGap(227, 227, 227)
                                .addComponent(play)
                                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(exit)
                                        .addComponent(tongtien, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(tien, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(20, 20, 20)
                                .addComponent(dia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(play)
                                .addGap(10, 10, 10)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(tbau)
                                        .addComponent(tcua)
                                        .addComponent(ttom)
                                        .addComponent(xbau, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(xcua, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(xtom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(tom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(cua, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(bau, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(tca)
                                        .addComponent(tnai)
                                        .addComponent(tga)
                                        .addComponent(xca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(xnai, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(xga, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(5, 5, 5)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(ga, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(nai, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(ca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap())
        );

        pack();
    }// </editor-fold>                        

//    private void playActionPerformed(java.awt.event.ActionEvent evt) {
//        if (check == 0) {
//            play.setText("OK");
//            check = 1;
//            xx1.setVisible(true);
//            xx2.setVisible(true);
//            xx3.setVisible(true);
//            System.out.println("Xuc Xac");
//            for (int i = 0; i < 3; i++) {
//                xx[i] = new Random().nextInt(6);
//                String s = "";
//                switch (xx[i]) {
//                    case 0:
//                        s = "BAU";
//                        break;
//                    case 1:
//                        s = "CUA";
//                        break;
//                    case 2:
//                        s = "TOM";
//                        break;
//                    case 3:
//                        s = "CA";
//                        break;
//                    case 4:
//                        s = "NAI";
//                        break;
//                    case 5:
//                        s = "GA";
//                        break;
//                }
//                if (i == 0) {
//                    xx1.setText(s);
//                }
//                if (i == 1) {
//                    xx2.setText(s);
//                }
//                if (i == 2) {
//                    xx3.setText(s);
//                }
//            }
//            int tien = this.user.getMoney();
//            tien += cuoc[xx[0]] + cuoc[xx[1]] + cuoc[xx[2]];
//            this.user.setMoney(tien);
//            for (int i = 0; i < 6; i++) {
//                cuoc[i] = 0;
//            }
//            tongtien.setText("$$: " + this.user.getMoney());
//            xbau.setText("x0");
//            xcua.setText("x0");
//            xtom.setText("x0");
//            xca.setText("x0");
//            xnai.setText("x0");
//            xga.setText("x0");
//        } else {
//            play.setText("Play");
//            check = 0;
//            xx1.setVisible(false);
//            xx2.setVisible(false);
//            xx3.setVisible(false);
//        }
//    }
    private void bauActionPerformed(java.awt.event.ActionEvent evt) throws IOException {
        System.out.println("Dat cuoc bau");
        if (this.user.getMoney() >= 100) {
            cuoc[0] += 100;
            xbau.setText("x" + cuoc[0] / 100);
            int tien = this.user.getMoney();
            tien -= 100;
            dos.writeUTF("mess");
            dos.writeUTF("bau");
            dos.writeInt(100);
            this.user.setMoney(tien);
            tongtien.setText("$$: " + this.user.getMoney());
        } else {
            JOptionPane.showMessageDialog(null, "Khong du tien dat cuoc", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void cuaActionPerformed(java.awt.event.ActionEvent evt) throws IOException {
        if (this.user.getMoney() >= 100) {
            System.out.println("Dat cuoc cua");
            cuoc[1] += 100;
            xcua.setText("x" + cuoc[1] / 100);
            int tien = this.user.getMoney();
            tien -= 100;
            dos.writeUTF("mess");
            dos.writeUTF("cua");
            dos.writeInt(100);
            this.user.setMoney(tien);
            tongtien.setText("$$: " + this.user.getMoney());
        } else {
            JOptionPane.showMessageDialog(null, "Khong du tien dat cuoc", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void tomActionPerformed(java.awt.event.ActionEvent evt) throws IOException {
        if (this.user.getMoney() >= 100) {
            System.out.println("Dat cuoc tom");
            cuoc[2] += 100;
            xtom.setText("x" + cuoc[2] / 100);
            int tien = this.user.getMoney();
            tien -= 100;
            dos.writeUTF("mess");
            dos.writeUTF("tom");
            dos.writeInt(100);
            this.user.setMoney(tien);
            tongtien.setText("$$: " + this.user.getMoney());
        } else {
            JOptionPane.showMessageDialog(null, "Khong du tien dat cuoc", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void caActionPerformed(java.awt.event.ActionEvent evt) throws IOException {
        if (this.user.getMoney() >= 100) {
            System.out.println("Dat cuoc ca");
            cuoc[3] += 100;
            xca.setText("x" + cuoc[3] / 100);
            int tien = this.user.getMoney();
            tien -= 100;
            dos.writeUTF("mess");
            dos.writeUTF("ca");
            dos.writeInt(100);
            this.user.setMoney(tien);
            tongtien.setText("$$: " + this.user.getMoney());
        } else {
            JOptionPane.showMessageDialog(null, "Khong du tien dat cuoc", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void naiActionPerformed(java.awt.event.ActionEvent evt) throws IOException {
        if (this.user.getMoney() >= 100) {
            System.out.println("Dat cuoc nai");
            cuoc[4] += 100;
            xnai.setText("x" + cuoc[4] / 100);
            int tien = this.user.getMoney();
            tien -= 100;
            dos.writeUTF("mess");
            dos.writeUTF("nai");
            dos.writeInt(100);
            this.user.setMoney(tien);
            tongtien.setText("$$: " + this.user.getMoney());
        } else {
            JOptionPane.showMessageDialog(null, "Khong du tien dat cuoc", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void gaActionPerformed(java.awt.event.ActionEvent evt) throws IOException {
        if (this.user.getMoney() >= 100) {
            System.out.println("Dat cuoc ga");
            cuoc[5] += 100;
            xga.setText("x" + cuoc[5] / 100);
            int tien = this.user.getMoney();
            tien -= 100;
            dos.writeUTF("mess");
            dos.writeUTF("ga");
            dos.writeInt(100);
            this.user.setMoney(tien);
            tongtien.setText("$$: " + this.user.getMoney());
        } else {
            JOptionPane.showMessageDialog(null, "Khong du tien dat cuoc", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void exitActionPerformed(java.awt.event.ActionEvent evt) {
        int dialog = JOptionPane.YES_NO_OPTION;
        int res = JOptionPane.showConfirmDialog(this, "Thoát", "Bạn muốn thoát khỏi phòng?", dialog);
        if (res == JOptionPane.YES_OPTION) {
            this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            try {
                dos.writeUTF("userOutRoom");
                String leave = user.getName() + " đã thoát ...";
                dos.writeUTF(leave);
                dos.writeInt(user.getMoney());
                InRoomGame.remove(roomName);
                UserView u = new UserView(user, socketUV);
                u.setVisible(true);
                this.dispose();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } else {
            this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        }
    }

    public static void main(String args[]) {
        // java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new Front().setVisible(true);
//            }
        //   });
    }

    // Variables declaration - do not modify                     
    private javax.swing.JButton bau;
    private javax.swing.JButton cua;
    private javax.swing.JButton tom;
    private javax.swing.JButton ca;
    private javax.swing.JButton nai;
    private javax.swing.JButton ga;
    private javax.swing.JButton play;
    private javax.swing.JButton exit;
    private javax.swing.JLabel xbau;
    private javax.swing.JLabel xcua;
    private javax.swing.JLabel xtom;
    private javax.swing.JLabel xca;
    private javax.swing.JLabel xnai;
    private javax.swing.JLabel xga;
    private javax.swing.JLabel tongtien;
    private javax.swing.JLabel tenuser;
    private javax.swing.JLabel xx1;
    private javax.swing.JLabel xx2;
    private javax.swing.JLabel xx3;
    private javax.swing.JLabel tien;
    private javax.swing.JLabel tbau;
    private javax.swing.JLabel tcua;
    private javax.swing.JLabel ttom;
    private javax.swing.JLabel tca;
    private javax.swing.JLabel tnai;
    private javax.swing.JLabel tga;
    private ImagePanel dia;
    // End of variables declaration                   
}

class ImagePanel extends JPanel {

    private Image img;

    public ImagePanel(String img) {
        this(new ImageIcon(img).getImage());
    }

    public ImagePanel(Image img) {
        this.img = img;
        Dimension size = new Dimension(img.getWidth(null), img.getHeight(null));
        setPreferredSize(size);
        setMinimumSize(size);
        setMaximumSize(size);
        setSize(size);
        setLayout(null);
    }

    public void paintComponent(Graphics g) {
        g.drawImage(img, 0, 0, null);
    }

}
