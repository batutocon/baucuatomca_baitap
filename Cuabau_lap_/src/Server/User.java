package Server;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
/**
 *
 * 
 */
public class User implements Serializable{
    private String name;
    private int money;
    public User(String name, int money) {
        this.name = name;
        this.money = money;
    }
    public User() {
        
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public String getName() {
        return name;
    }

    public int getMoney() {
        return money;
    }    
}
