package Server;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import Server.Server;
import Server.ServerRoom;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * 
 */
public class Room extends Thread {

    public Room(Server server, String roomname) {
        this.roomname = roomname;
        this.server = server;
        this.userInRoom = new HashSet<ServerRoom>();
    }

    public String getRoomname() {
        return roomname;
    }

    public void setRoomname(String roomname) {
        this.roomname = roomname;
    }

    public Server getServer() {
        return server;
    }

    public void setServer(Server server) {
        this.server = server;
    }
    private String roomname;
    private HashSet<ServerRoom> userInRoom;
    private Server server;

    public void run() {
        while (true) {
            for (ServerRoom tmp : userInRoom) {
                tmp.reset();
            }
            try {
                Thread.sleep(5000);
                String mess = "stop";
                server.broadCastRoom(mess, roomname);
                Thread.sleep(5000);
                int xx1 = new Random().nextInt(6);
                int xx2 = new Random().nextInt(6);
                int xx3 = new Random().nextInt(6);
                System.out.println(xx1 + " " + xx2 + " " +xx3);
                for (ServerRoom tmp : userInRoom) {
                    int money = tmp.getType(xx1) * 2 + tmp.getType(xx2) * 2 + tmp.getType(xx3) * 2;
                    System.out.println(money);
                    tmp.getXxRs(xx1, xx2, xx3);
                    tmp.getMoneyBack(money);
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(Room.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Room.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public int numberUserInRoom() {
        return userInRoom.size();
    }

    public void addUser(ServerRoom e) {
        this.userInRoom.add(e);
    }

    public HashSet<ServerRoom> getUserInRoom() {
        return userInRoom;
    }

    public void removeUser(ServerRoom srm) {
        userInRoom.remove(srm);
    }
}
