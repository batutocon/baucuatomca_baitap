package Server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * 
 */
public class ServerRoom extends Thread {

    private User you;
    private String roomName;
    private Server server;
    private Socket socket;
    private DataInputStream dis;
    private DataOutputStream dos;
    private int[] type;

    public ServerRoom(User you, String room, Server server, Socket socket) throws IOException {
        this.you = you;
        this.roomName = room;
        this.server = server;
        this.socket = socket;
        dis = new DataInputStream(socket.getInputStream());
        dos = new DataOutputStream(socket.getOutputStream());
        type = new int[6];
    }

    public void run() {
        // listUserInEveryRoom();
        while (true) {
            try {
                String type = dis.readUTF();
                System.out.println(type);
                if (type.equals("mess")) {
                    String xxdat = dis.readUTF();
                    int xxtiendat = dis.readInt();
                    System.out.println(xxdat + " " + xxtiendat);
                    if (xxdat.equals("bau")) {
                        this.type[0] += xxtiendat;
                    } else if (xxdat.equals("cua")) {
                        this.type[1] += xxtiendat;

                    } else if (xxdat.equals("tom")) {
                        this.type[2] += xxtiendat;

                    } else if (xxdat.equals("ca")) {
                        this.type[3] += xxtiendat;

                    } else if (xxdat.equals("nai")) {
                        this.type[4] += xxtiendat;

                    } else if (xxdat.equals("ga")) {
                        this.type[5] += xxtiendat;
                    }

                    //                  server.broadCastRoom(this, mess, roomName, you);
//                    System.out.println(mess + " " + roomName);
                }
                if (type.equals("userOutRoom")) {
                    String mess = dis.readUTF();
                    int money = dis.readInt();
                    you.setMoney(money);
                 //   server.broadCastRoom(mess, roomName);
                    userOutRoom();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
                break;
            }

        }
    }

    public User getYou() {
        return you;
    }

    public Socket getSocket() {
        return socket;
    }

    public String getRoomName() {
        return roomName;
    }

    public void getMess(String mess) throws IOException {
        dos.writeUTF("mess");
        dos.writeUTF(mess);
    }

    public void getMoneyBack(int money) throws IOException {
        dos.writeUTF("MoneyBack");
        dos.writeInt(money);
    }

    public void getXxRs(int xx1, int xx2, int xx3) throws IOException {
        dos.writeUTF("XxBack");
        dos.writeInt(xx1);
        dos.writeInt(xx2);
        dos.writeInt(xx3);

    }

    public void reset() {
        this.type = new int[6];
    }

    public int getType(int i) {
        return type[i];
    }

    public void listUserInRoom() throws IOException {
        dos.writeUTF("listUser");
        Room room = server.getRoom(roomName);
        HashSet<ServerRoom> srm = room.getUserInRoom();
        dos.writeInt(srm.size());
        for (ServerRoom tmp : srm) {
            dos.writeUTF(tmp.getYou().getName());
            dos.writeInt(tmp.getYou().getMoney());
        }
    }

    public void listUserInEveryRoom() {
        try {
            Room e = server.getRoom(roomName);
            HashSet<ServerRoom> srm = e.getUserInRoom();
            for (ServerRoom tmp : srm) {
                tmp.listUserInRoom();
            }
        } catch (IOException ex) {
        }
    }

    private void userOutRoom() {
        Room e = server.getRoom(roomName);
        e.removeUser(this);
       // listUserInEveryRoom();
        try {        this.stop();

            
        } catch (Exception ex) {
            System.err.println(ex);
        }
    }

}
