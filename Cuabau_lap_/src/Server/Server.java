package Server;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;

/**
 *
 * 
 */
public class Server {

    public int port;
    public HashSet<ServerControl> listControl;
    public ArrayList<Room> roomGame;

    public Server(int port) {
        this.port = port;
        listControl = new HashSet<>();
        roomGame = new ArrayList<>();
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public Room getRoom(String roomName) {
        for (Room room : roomGame) {
            if (room.getRoomname().equals(roomName)) {
                return room;
            }
        }
        return null;
    }

    public HashSet<ServerControl> getListControl() {
        return listControl;
    }

    public void setListControl(HashSet<ServerControl> listControl) {
        this.listControl = listControl;
    }

    public void run() {
        try {
            ServerSocket serverSocket = new ServerSocket(port);
            while (true) {
                Socket con = serverSocket.accept();
                DataInputStream dis = new DataInputStream(con.getInputStream());
                DataOutputStream dos = new DataOutputStream(con.getOutputStream());
                String choice = dis.readUTF();
                if (choice.equals("newUser")) {
                    insertUser(con, dis, dos);
                }
                if (choice.equals("addUserToRoom")) {
                    addUserToRoom(con, dis, dos);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void insertUser(Socket con, DataInputStream dis, DataOutputStream dos) throws IOException {
        User user = new User(dis.readUTF(), 1000);
        int mark = 0;
        for (ServerControl tmp : listControl) {

            if (tmp.getUser().getName().equals(user.getName())) {
                mark = 1;
                break;
            }
        }
        dos.writeInt(mark);
        if (mark == 1) {
            return;
        }
        ServerControl sc = new ServerControl(this, con, user);
        listControl.add(sc);
        sc.start();

    }

    public void removeUser(ServerControl tmp) {
        listControl.remove(tmp);
    }

    private void addUserToRoom(Socket con, DataInputStream dis, DataOutputStream dos) throws IOException {
        User user = new User();
        user.setName(dis.readUTF());
        user.setMoney(1000);

        String roomName = dis.readUTF();
        ServerRoom sr = new ServerRoom(user, roomName, this, con);
        System.out.println(roomName);
        sr.start();
        for (Room room : roomGame) {
            if (room.getRoomname().equals(roomName)) {
                room.addUser(sr);
                break;
            }
        }
        System.out.println("add User " + user.getName() + " to Room " + roomName);
    }

    void broadCastRoom(String mess, String roomName) throws IOException {
        for (Room room : roomGame) {
            if (room.getRoomname().equals(roomName)) {
                HashSet<ServerRoom> x = room.getUserInRoom();
                for (ServerRoom romMessage : x) {
                    System.out.println(romMessage.getYou());
                    
                    romMessage.getMess(mess);
                }
            }
        }
    }

//    void broadCastRoom(Socket con, ServerRoom aThis, String mess, String roomName, User user) throws IOException {
//        for (Room room : roomGame) {
//            if (room.getUser().getName().equals(roomName)) {
//                HashSet<ServerRoom> x = room.getUserInRoom();
//                for (ServerRoom romMessage : x) {
//                    System.out.println(romMessage.getYou());
//                    if (user.getName().equals(romMessage.getYou().getName())) {
//                        BufferedReader is;
//                        BufferedWriter os;
//                        is = new BufferedReader(new InputStreamReader(con.getInputStream()));
//                        os = new BufferedWriter(new OutputStreamWriter(con.getOutputStream()));
//                        String money = Integer.toString(user.getMoney());
//                        os.write(money);                          
//                        os.flush();
//                        continue;
//                    }
//                    romMessage.getMess(mess);
//                }
//            }
//        }
//    }

}
